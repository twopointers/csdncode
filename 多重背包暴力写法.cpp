//多重背包问题暴力写法
#include <iostream>
#include <algorithm>

using namespace std;

const int N = 110;
int n,m;
int v[N],w[N],s[N];
int f[N][N];

int main()
{
    //输入物品数量和背包体积
    cin >> n >> m;
    //录入每个物品的体积和价值信息
    for(int i = 1; i <= n;i++) cin >> v[i] >> w[i]>> s[i];

    //处理所有状态
    for(int i = 1; i <= n;i++)
        for(int j = 0; j <= m;j++)
            for(int k = 0; k <= s[i] && k * v[i] <= j;k++)
                f[i][j] = max(f[i][j],f[i-1][j-v[i]*k] + w[i]*k);

    cout << f[n][m] <<endl;

    return 0;
}
