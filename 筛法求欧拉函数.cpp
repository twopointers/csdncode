#include <iostream>
#include <algorithm>

using namespace std;
typedef long long LL;//可能会报int溢出，所以放一个long long

const int N = 1000010;
int phi[N]; //存放欧拉函数的数组
int primes[N],cnt;
bool st[N];


LL get_eulaers(int n)
{
    phi[1] = 1;
    //写线性筛法的模板
    for(int i = 2; i <=n;i++)
    {
        if(!st[i])
        {
           primes[cnt++] =  i;//如果当前这个i没有被筛选过，说明是素数，放到primes数组中
           phi[i] = i-1;
        }

         //从小到大遍历每一个素数，用它来筛选
         for(int j = 0; primes[j] <= n/i;j++)
         {
             st[primes[j] *i ] = true;
             if(i % primes[j] == 0) 
             {
                 phi[primes[j] * i] = phi[i] * primes[j];
                 break;
             }
             phi[primes[j] * i] = phi[i]*(primes[j] - 1);
         }
    }

    LL res = 0;
    for(int i = 1; i <= n;i++) res += phi[i];
    return res;

}

int main()
{
    //输入
    int n;
    cin >> n;

	//调用函数，比赛的时候，函数名直接写f都可以，保证可以Ac就好
    cout <<get_eulaers(n) << endl;

    return 0;
}
