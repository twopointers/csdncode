#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;

const int N = 5000010;
int h[N][3];
int n;

int main(int argc, char** argv) {
    cin >> n;
    for(int c = 0; c*c <= n; c++)
        for(int d = c; d*d + c*c <= n; d++){
            int tmp = d*d + c*c;
            //哈希表中，当前位置没有数据才记录，倘若冲突了，也可以保证优先记录字典序较小的
            if(h[tmp][0] == 0) {
                h[tmp][0] = tmp;
                h[tmp][1] = c;
                h[tmp][2] = d;
            }
        }
    for(int a = 0; a*a <= n; a++)
        for(int b = a; b*b + a*a <= n; b++) {
            int ans = n - a*a-b*b;
            if(h[ans][0] != 0){
                cout << a << " " << b << " " << h[ans][1] << " " << h[ans][2];
                return 0;
            }
        }
    return 0;
}

