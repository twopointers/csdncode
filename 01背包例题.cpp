//朴素版本优化
#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1010;
int n,m;//n表示物体数量，m表示背包容量
int v[N],w[N];//v表示体积，w表示状态
int f[N][N];//表示状态

int main()
{
    //输入环节
    //读入物体个数和容量
    cin >>n >>m;

    //读入物品的信息
    for(int i = 1; i <= n;i++) cin >> v[i] >> w[i];

    //初始化枚举所有状态。因为f[0][0-m]是一个都没有选的状态，所有方案都是0，就直接从1开始枚举
    for(int i = 1; i <= n;i++)
        for(int j = 0; j <= m;j++)//不含i的情况是一定存在的，而含i的情况不一定存在
        {
            //图左边不选第i个物品的情况是一定存在
            f[i][j] = f[i-1][j];
            //图右边选第i个物品的情况不一定存在,只有当j >= v[i]的时候才存在
            if(j  >= v[i] ) //推导的状态转移方程放进来
            {
                f[i][j] = max(f[i][j],f[i-1][j-v[i]]+w[i]);
            }
        }
    cout << f[n][m] <<endl;

    return 0;
}


//滚动数组优化
#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1010;
int n,m;//n表示物体数量，m表示背包容量
int v[N],w[N];//v表示体积，w表示状态
int f[2][N];//表示状态

int main()
{
    //输入环节
    //读入物体个数和容量
    cin >>n >>m;

    //读入物品的信息
    for(int i = 1; i <= n;i++) cin >> v[i] >> w[i];

    //初始化枚举所有状态。因为f[0][0-m]是一个都没有选的状态，所有方案都是0，就直接从1开始枚举
    for(int i = 1; i <= n;i++)
        for(int j = 0; j <= m;j++)//不含i的情况是一定存在的，而含i的情况不一定存在
        {
            //图左边不选第i个物品的情况是一定存在
            f[i&1][j] = f[(i-1)&1][j];
            //图右边选第i个物品的情况不一定存在,只有当j >= v[i]的时候才存在
            if(j  >= v[i] ) //推导的状态转移方程放进来
            {
                f[i&1][j] = max(f[i&1][j],f[(i-1)&1][j-v[i]]+w[i]);
            }
        }

    int ans = 0;
    for(int j = 0; j <= m;j++) ans = max(ans,f[n&1][j]);
    cout << ans << endl;
    return 0;
}

//一维优化
#include <iostream>
#include <algorithm>

using namespace std;
const int N  = 1010;
int n,m;
int v[N],w[N];
int f[N];


int main()
{
    cin >> n >>m;
    for(int i =1; i <= n;i++) cin >> v[i] >> w[i];

    for(int i = 1;i <= n;i++)
        for(int j = m;j>= v[i];j--)
        f[j] = max(f[j],f[j-v[i]]+w[i]);

    cout << f[m] << endl;

    return 0;
}

