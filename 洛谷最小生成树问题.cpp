#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;
const int N = 5010, M = 2e5 + 10;
int p[N];
int n,m,cnt,res;

struct Edge{
    int a,b,w;
}e[M];

bool cmp(Edge a, Edge b)
{
    return a.w < b.w;
}

int find(int x)
{
    return p[x] == x?x:p[x] = find(p[x]);
}

//补kruskal算法
void kruskal()
{
    //排序
    sort(e,e+m,cmp);
    //初始化并查集
    for(int i = 1; i <= n;i++) p[i] = i;

    //从小到大枚举所有边
    for(int i = 0; i < m;i++)
    {
        int a = find(e[i].a) , b = find(e[i].b);
		//倘若未连通，将它们连通
        if(a != b)
        {
            //连通
            p[a] = b;
            cnt ++;
            res += e[i].w;
        }
    }

}

//主函数
int main()
{
    //输入
    scanf("%d %d",&n,&m);
    //建图
    for(int i = 0; i < m;i++)
    {
        scanf("%d %d %d",&e[i].a,&e[i].b,&e[i].w);
    }
    //调用函数
    kruskal();

    //输出
    if(cnt < n-1) puts("orz");
    else printf("%d\n",res);
    return 0;
}
