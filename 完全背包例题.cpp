//朴素版
#include <iostream>
#include <algorithm>

using namespace std;
const int N = 1010;

int n,m;
int v[N],w[N];
int f[N][N];


int main()
{
    ios::sync_with_stdio(false);
    cin >> n >>m;
    for(int i  = 1; i <= n; i++) cin >>v[i] >> w[i];

    for(int i = 1;i <= n;i++)
        for(int j = 0; j <= m;j++)
        {
            f[i][j] = f[i-1][j];
            if(j >= v[i]) f[i][j] = max(f[i-1][j],f[i][j-v[i]]+w[i]);
        }

    cout << f[n][m]  << endl;
    return 0;
}


//一维优化
#include <iostream>
#include <algorithm>

using namespace std;
int n, m;
const int N = 1010;
int v[N] , w[N];
int f[N];


int main()
{
    ios::sync_with_stdio(false);
    cin >> n >>m;

    for(int i = 1; i <= n; i++) cin >> v[i] >> w[i];
    for(int i = 1;i <= n;i++)
        for(int j = v[i]; j <= m;j++)
        {
            f[j] = f[j];
            //f[j] = f[j] 。这个等式是先算右边，假如去掉第一维，此时右边的f[j]就是上一层i-1算出来的值，也就是f[i-1][j]
            f[j] =  max(f[j],f[j - v[i]] + w[i]);
            //f[j] = max(f[j], f[j-v[i]] + w[i]) f[i-1][j]是等价的，对于后面，因为j从小到大，所有j - v[i]是小于j的
            //当要算j的时候，需要的j- v[i]已经在i层中算出来了，现在的循环就是i层中，所以可以直接去掉
        }

    cout << f[m] << endl;

}
