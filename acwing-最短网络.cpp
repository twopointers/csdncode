#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;

const int N = 110,INF = 0x3f3f3f3f;

int g[N][N];
int n,res;
int dist[N];
bool st[N];

int prim()
{
    //初始化距离数组
    memset(dist,0x3f,sizeof dist);
    dist[1]= 0;

    //去处理这n个点，每次寻找权值最小的点
    for(int i = 0; i < n ;i++)
    {
        int t = -1;
        for(int j = 1;j <= n;j++)
            if(!st[j] && (t==-1 || dist[t] > dist[j]))
            t = j;


        st[t] = true; //标记这个点已经被纳入最小生成树的集合
        res += dist[t];

            for(int j = 1;j <=n;j++)
            dist[j] = min(dist[j],g[t][j]);
    }
    return res;
}


int main()
{
    //输入
    scanf("%d",&n);
    for(int i = 1;i <= n;i++)
        for(int j = 1;j<=n;j++) cin >>g[i][j];

    //调用函数
    int res = prim();

    //输出结果
    printf("%d\n",res);
    return 0;

}

