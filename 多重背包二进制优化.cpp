//多重背包的二进制优化版本
#include <iostream>
#include <algorithm>

using namespace std;
//1000 * log2000 约等于 12000
const int N  = 15000, M = 2010;

int n,m;
int v[N],w[N];
int f[N];

int main()
{
    cin >> n>> m;

    int cnt = 0;
    //读入当前物品的体积、价值、个数
    for(int i = 1; i <= n;i++)
    {
        int a,b,s;
        cin >>a >> b>>s;

        int k =1;
        //进行打包的过程,打包的时候，对每组的体积，价值也进行相应拓展，也就是乘以这个包裹中物品的个数
        while( k <= s)
        {
            cnt ++;
            v[cnt] = a*k;
            w[cnt] = b*k;
            //注意扣除s和拓展k
            s -= k;
            k *= 2;
        }
        //这里就是2的整数幂处理不了的，我们手动打包为X的部分
        if(s > 0)
        {
            cnt ++;
            v[cnt] = a *s;
            w[cnt] = b*s;
        }
    }

    n = cnt;
    //做n组01背包
    for(int i = 1; i <= n;i++)
    {
        for(int j = m; j >= v[i];j--)
            f[j] = max(f[j],f[j-v[i]] + w[i]);
    }

    cout << f[m] << endl;
    return 0;
}
