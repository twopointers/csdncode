#include <iostream>

using namespace std;


int exgcd(int a, int b,int &x, int &y)
{
    if(!b)
    {
        //处理边界
        x = 1, y = 0;
        return a;//即，如果b是零，一个不是零的a与0的最大公约数就是a
    }
    //递归
    int d =  exgcd(b,a%b,y,x);
    y -= a / b * x;
    return d;
}

int main()
{
    //输入
    int n;
    scanf("%d",&n);

    //调用函数+输出
    while(n--)
    {
        int a,b,x,y;
        scanf("%d%d",&a,&b);

        exgcd(a,b,x,y);
        // 配出来的系数 x 和 y 是不唯一的喔
        printf("%d %d\n",x,y);
    }
    return 0;
}
