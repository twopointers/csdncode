#include <cstdio>
#include <algorithm>
using namespace std;

double const MAX = 10000000000000.0;
int n, m, tmp[1010], p[60];
double ans;
//存储边的信息的结构体
struct Edge
{
    int u, v;
    double w, val;
}e[1010];

//对比较的方式进行定义，使比较的方式是按照权值w进行比较
bool cmp(Edge a, Edge b)
{
    return a.w < b.w;
}

//并查集的核心函数
int Find(int x)
{
    return x == p[x] ? x : p[x] = Find(p[x]);
}

//将两个生成树集合连通的函数
void Union(int a, int b)
{
    int r1 = Find(a);
    int r2 = Find(b);
    if(r1 != r2)
        p[r2] = r1;
}

//可爱的Kruskal算法
void Kruskal(int sum)
{
	//初始化并查集
    for(int i = 1; i <= n; i++)
        p[i] = i;
        
    int cnt = 0;
    double sum_w = 0;
    double sum_val = 0;
	//求平均数
    double ave = sum * 1.0 / (n - 1);
    //遍历每一条边，算(xi-平均数)^2
    for(int i = 0; i < m; i++)
        e[i].w = (e[i].val - ave) * (e[i].val - ave);
	//重新排序
    sort(e, e + m, cmp);
    for(int i = 0; i < m; i++)
    {
        int u = e[i].u;
        int v = e[i].v;
        if(Find(u) != Find(v))
        {
        	//联结当前的最小生成树集合
            Union(u, v);
            sum_w += e[i].w;
            sum_val += e[i].val;
            cnt ++;
        }

		//到达生成树的边数限制了
        if(cnt == n - 1)
            break;
    }
	//找到合适的枚举情况了，返回当前计算的（xi - avg）
    if((int)sum_val == sum)
        ans = min(ans, sum_w);
}

int main()
{
	//输入环节
    int ca = 1;
    while(scanf("%d %d", &n, &m) != EOF && (m + n))
    {
        int minv = 0;
        int maxv = 0;
        ans = MAX;
		//构建图
        for(int i = 0; i < m; i++)
        {
            scanf("%d %d %lf", &e[i].u, &e[i].v, &e[i].val);
            tmp[i] = e[i].val;//将当前的权值存放到这个数组中
        }
        //根据当前的权值排序，待会用于计算图的边权值之和
        sort(tmp, tmp + m);
        
		//圈定枚举的范围，定可能出现的权值之和的最小值和最大值，形成一个区间，用于枚举
        //统计n-1条边的最小权值和最大权值。
        for(int i = 0; i < n - 1; i++)
            minv += tmp[i];
        for(int i = m - 1; i > m - n; i--)
            maxv += tmp[i];
		//minv就是可能出现的权值和的最小值，maxv则是可能出现的权值之和的最大值
        for(int i = minv; i <= maxv; i++)
            Kruskal(i);

		//输出结果
        ans = ans / (n - 1);
        printf("Case %d: %.2f\n", ca++, ans);
    }
}
